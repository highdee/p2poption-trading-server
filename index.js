var ProcessIncomingTradeDate = require('./services/process-incoming-trade-data');
var fileService  = require('./services/file-service');
const Trade = require('./services/trade');

var fs = require('fs');
var http = require('http');
var https = require('https');
const express = require('express');
const io_server = require('socket.io');
const WebSocket = require('ws').Server;
const io_client = require("socket.io-client");
const cors = require('cors');
const { userInfo } = require('os');
const { exec } = require('child_process');

// CORS ERROR
const options = {
    key: fs.readFileSync("./ssl/cert.key"),
    cert: fs.readFileSync("./ssl/chained.crt")
};

var server = express();
// var httpserver = https.createServer(options,server).listen(3000);
server.use(cors())

var socket_connections = {}; //{2:socket_instance}
var users_connection_instances={}; //{2:[socket_instance1,socket_instance2]}
// INITIALISING SOCKET FOR LIVE RATE CONNECTIONS
const socket_client = io_client('https://wss3.live-rates.com/', {transports: ['websocket']});

// INITIALISING SOCKET FOR FRONTEND CONNECTIONS
http = http.Server(server);
http = http.listen('3001');
const socket_server = new WebSocket({server:http});


socket_server.on('connection', (socket,req) => {
    var userID = req.url.replace('/','');
    socket.isAlive = true;

    // SAVING THIS SOCKET CONNECTION INSTANCE FOR THIS USER (A SINGLE USER CAN HAVE MULTIPLE CONNECTION INSTANCES IN THE SERVER)
    if(!users_connection_instances[userID]) users_connection_instances[userID]=[];
    users_connection_instances[userID].unshift(socket);

    
    Trade.updateUserConnectionInstances(users_connection_instances);

    console.log('User '+userID+' connected');

    socket.on('message',(e)=>{
        var data = JSON.parse(e);
        switch(data.action){
            case 'order':
                Trade.takeOrder(data); //RECEIVE ORDER FROM USER FOR A TRADE
                break;
            default :
                console.log('Socket message has no action')
        }
    });

    socket.on('close',()=>{
        socket.isAlive = false;
        // remove socket from user list of instances since its been disconnected
        try{users_connection_instances[userID] = users_connection_instances[userID].filter((ws)=> ws.isAlive);}
        catch(e){
            //
        }
        Trade.updateUserConnectionInstances(users_connection_instances);
        console.log(userID+' just disconnected');
    });
});


var ItradeP = new ProcessIncomingTradeDate();

// CONNECTION TO LIVE RATE SOCKET WAS SUCCESSFULL
socket_client.on("connect", () => {
    console.log('CONNECTED TO LIVE RATE SOCKET TRADING SERVER......'); 

    // SUBSCRIBE TO SYMBOLS
    var instruments = ['GBPAUD','GBPCAD','EURUSD','BTCUSD'];
    socket_client.emit('instruments', instruments);
    socket_client.emit('key', '4b5d65f334');
});

// CONNECTION ERROR OCCURED
socket_client.on('connect_error', function(){
    console.log('Connection error. If you see this message for more then 15 minutes then contact us. ');
});

// WHEN RATE ARRIVES FROM LIVE RATES
socket_client.on('rates', function(msg) {
    msg = JSON.parse(msg); 
    // if its a trade data
    if(msg.currency){
        // SAVE TRADE DATA
        ItradeP.process_data(users_connection_instances,msg);

        // NEW DATA, LETS CONTINUE TRADING
        Trade.trade(msg);
    } 
});


// GET HISTORICAL DATA OF SPECIFIED RATES
server.get('/rates/:currency',function(req,res){
    // console.log(req.ip+' requested for history');
    var currency = req.params.currency;
    var result  = fileService.getCurrencyRate(currency);
    return res.json(result);
});

server.listen(3000,function(){
    console.log('Server running....');
})