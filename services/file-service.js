var fs = require('fs');
var path = require('path');
var Variables  = require('../services/variables'); 


module.exports = class FileService {

    static writeFile(filename,data){
        fs.writeFile(filename,data,(error)=>{
            if(error) console.log(error);

            console.log('File '+filename+' was saved successfully');
        });
    }
    static readOrResetJsonFile(filename){
        try{
            var content = fs.readFileSync(filename,{encoding:'utf8', flag:'r'});
            if(typeof content != 'string'){
                return {};
            }
            content = JSON.parse(content);
            console.log('Json file was read successfully.');
            return content
        }catch(e){
            console.log("Unable to read json file.");
            return {};
        }
        
    }
    static getCurrencyRate(currency){
        var data = FileService.readOrResetJsonFile(Variables.JSON_FILE_PATH);
        if(data[currency]){
            return data[currency];
        }
        return [];
    }
}