var fileService  = require('../services/file-service');
var Variables  = require('../services/variables'); 
const { trade } = require('./trade');

module.exports = class IncomingTradeDataProcessor {

    constructor(){ 
        this.new_data_arrived =  false;
        // BELOW VARIABLES HELP ME TRACK TIME AND VALUE SO I DON'T KEEP SAVING TRADE DATA OF A MINUTE SEVERAL TIME (SINCE WHAT WE NEED IN FRONTEND IS FINAL PRICE)
        // SO I CAN USE THIS VARIABLES TO KNOW THE FINAL VALUE TO ADD TO THE JSON FILE 
        this.asset_time_tracker = {};
        this.asset_value_tracker = {}

        // OPEN THE TRADE JSON FILE FOR READING
        this.trade_data = fileService.readOrResetJsonFile(Variables.JSON_FILE_PATH);
        // var context=this;
        setInterval(()=>{
            if(this.new_data_arrived){
                fileService.writeFile(Variables.JSON_FILE_PATH,JSON.stringify(this.trade_data));
                this.new_data_arrived = false;
            }
        },1000 * 10);
    }

    process_data(socket_instances,data){ 
        if(data.currency){ // it is a trading data
            var currency = data.currency;
           
            // INITIALIZING THE ASSET ARRAY FOR THE FIRST TIME IF IT DOESN'T EXIST
            if(!this.asset_time_tracker[currency]) this.asset_time_tracker[currency]=null;

            // if open or close value is n/a
            if(data.open == 'n/a' || data.close == 'n/a') return false;

            if(!this.trade_data[currency]) this.trade_data[currency] = []; 
            // if(currency == 'BTCUSD') console.log(JSON.stringify(data));
            data = {...this.correct_data(data,currency)};    
            // if(currency == 'BTCUSD') console.log(data);    
            this.broadcast(socket_instances,data);    

            // LIVE RATE EMITS ALOT OF TRADE DATA PER MINUTE SO INSTEAD OF SAVING AROUD 60 TRADE OBJECT JUST FOR ONE MINUTE , NO!
            // WE WILL WAIT UNTIL WE DETECT THAT THE LAST MINUTE HAS ENDED THEN WE WILL SAVE THE TRADE DATA OF THE LAST MINUTE, THIS WILL HELP US TO SAVE MORE SPACE FOR THE JSON FILE
            // asset_value_tracker SAVES THE DATA PER TICK AND WHEN THE MINUTES END AND A NEW MINUTE STARTS THEN WE CAN PICKUP THE LAST DATA FROM asset_value_tracker
            // MIND YOU, THIS LOGIC DOESN'T AFFECT THE FRONTEND, EVERY TICK IS BEEN RECEIVED ON THE FRONTEND
            if(this.getTimeEndInMilli(this.asset_time_tracker[currency]) != this.getTimeEndInMilli(data.timestamp) && this.asset_value_tracker[currency] != undefined){

                this.trade_data[currency].push(this.asset_value_tracker[currency]);
                
                this.new_data_arrived = true; 
                this.asset_time_tracker[currency] = data.timestamp;

                data.high = data.open > data.close ? data.open : data.close;
                data.low = data.open < data.close ? data.open : data.close;
                

                // if trade data per currency is beyond our limit then reduce the array by slicing off the excess from behind
                if(this.trade_data[currency].length > Variables.ARRAY_SIZE_LIMIT){
                    var temp =[...this.trade_data[currency] ];
                    temp = temp.slice(-Variables.ARRAY_SIZE_LIMIT,-1);
                    this.trade_data[currency] = [...temp];
                    // console.log(currency+' size was reduced to '+ Variables.ARRAY_SIZE_LIMIT);
                } 
            }
            this.asset_value_tracker[currency] = data; 
        }
        return data;
    }
    correct_data(data,currency){
        var temp = {...data};
        var lastBar = this.asset_value_tracker[currency];
        
        if(!lastBar){
            // console.log('\First corrected');
            temp.open = temp.close;
            temp.high = temp.close;
            temp.low = temp.close;
            return temp
        }
        temp.open = lastBar.open; 
        temp.high = this.getHigh(lastBar,data);
        temp.low = this.getLow(lastBar,data);

        if(this.getTimeEndInMilli(lastBar.timestamp) != this.getTimeEndInMilli(temp.timestamp)) {
            // console.log('corrected');
            temp.open = lastBar.close;
            // temp.low = lastBar.high;
        }else{
            temp.open = lastBar.open;
            // temp.low = lastBar.low;
        }

        return temp;
    }
    getHigh(lastbar,data){
        var high = data.close > lastbar.high ? data.close : lastbar.high;
        return high;
    }
    getLow(lastbar,data){
        var low = data.close < lastbar.low ? data.close : lastbar.low;
        return low;
    }
    // getMax(data){

    // }
    getTimeEndInMilli(time){
        var date = new Date(time);
        var timeEnd= new Date(date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate() +' '+date.getHours()+':'+date.getMinutes()+':59');
        return timeEnd.getTime()
    }

    broadcast(connections,t_data){ 
       
        for(var index in connections){
            var instances = connections[index];
            instances.forEach((instance)=>{
                instance.send(JSON.stringify(t_data));
            });
        }
    }
    
}