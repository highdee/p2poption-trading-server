const request = require('request');
const variables = require('./variables');
const fileservice = require('./file-service');

const max_seconds = 59;
var users_connection_instances={}; //{2:[socket_instance1,socket_instance2]}
var all_trades = fileservice.readOrResetJsonFile(variables.TRADES_FILE_PATH);

// setTimeout(()=>{
//     fileservice.writeFile(variables.TRADES_FILE_PATH,JSON.stringify(all_trades));
// },2000);

module.exports = class Trade{
    static takeOrder(order){
        
        if(!order.quick_demo){
            // BALANCE VALIDATION
            var has_enough_balance = Trade.balanceValidation(order);
            if(!has_enough_balance) return false
        }

        // SAVE TRADES
        // delete order.user_token;
        if(!all_trades[order.user_id]){
            all_trades[order.user_id] = [];
        }
        all_trades[order.user_id].unshift(order);
        fileservice.writeFile(variables.TRADES_FILE_PATH,JSON.stringify(all_trades));
        
        // SENDING NOTIFICATIONS AND DATA
        Trade.notify(order.user_id,JSON.stringify({
            action:'trade_accepted',
            message:'Trade successfully placed at '+order.strike_price,
            trade:order
        }));
        
        Trade.notify(order.user_id,JSON.stringify({
            action:'update_trades',trades:all_trades[order.user_id]
        }));

        console.log("Order taken for user "+order.user_id);
    }
    static async balanceValidation(order){
        // checking user account balance
        const options = {
            url: variables.API_ENDPOINT+'/charge-user-balance/'+order.account_type+'/'+order.amount,
            headers: {"Authorization" : "Bearer "+order.user_token}
        };

        // IT RETURNS TRUE / FALSE 
        return await new Promise((resolve,reject)=>{
            request.get(options,function (error, response, body){
                console.log("Check User Balance http request error: "+error);
                if(response.statusCode == 200){
                    var result = JSON.parse(body);

                    Trade.notify(order.user_id,JSON.stringify({
                        action:'update_user',data:result
                    })); 
                    resolve(true);
                }
                else if(response.statusCode == 402){
                    Trade.notify(order.user_id,JSON.stringify({
                        action:'trade_notification',
                        type:'error',
                        message:'Sorry, You need to credit your wallet',
                        order_id:order.id
                    })); 
                    resolve(false);
                }
                else{
                    Trade.notify(order.user_id,JSON.stringify({
                        action:'trade_notification',
                        type:'error',
                        message:'we are unable to accept your trade due to verification error',
                        order_id:order.id
                    }));  
                    resolve(false);
                }
            });
        });
    }
    static trade(data){ 

        if((data.open == 'n/a' || data.close == 'n/a')) return false;
        
        var trades = {...all_trades}; 
        var tr =null;
        var time_diff = 0;
        // looping through each user trade array
        for(var index in trades){
            tr = trades[index];

        // looping through each user trade  
            trades[index] = tr.map((trade)=>{
                time_diff = 0;
                if(data.currency == trade.asset && trade.state != 'ended'){
                    time_diff = Trade.timeDiferenceInSeconds(data.timestamp,trade.purchase_time_ms);
                    
                    trade.time_used = Math.floor(time_diff);
                    trade.time_rem = trade.strike_second_rem - trade.time_used;
                    trade.time_rem = trade.time_rem < 0 ? (trade.trade_time - (trade.time_rem * -1)) : trade.time_rem;
                    trade.time_rem_formatted = Trade.formatTime(0, trade.time_rem); 
                    
                    // console.log((trade.time_rem),trade.total_active_session_seconds,trade.time_used);
                    
                    // FOR TRADES THAT HAS ENDED ALREADY BUT THE STATUS IS STILL WAITING DUE TO POSSIBLE MALFUNCTION OF SERVER ETC
                    if(trade.state != 'waiting' && (trade.time_rem) >= trade.total_active_session_seconds){
                        // TRADE HAS BEEN COMPLETED IN THE PAST
                        if(!trade.end_data){
                            trade.end_data=data;
                        }
                        trade.win = Trade.determineWinOrLose(trade.end_data.close,trade);
                        trade.state = 'ended';
                        Trade.submitTrade(trade);
                        return trade;
                    } 

                     // DETERMINE STATUS OF TRADE
                     if (trade.direction == 'up') {
                        if (data.close < trade.strike_price) {
                            trade.status = 'losing';
                        } else {
                            trade.status = 'winning';
                        }
                    } else if (trade.direction == 'down') {
                        if (data.close < trade.strike_price) {
                            trade.status = 'winning'; 
                        } else {
                            trade.status = 'losing';
                        }
                    }

                    if(trade.state == 'waiting'){
                        if((trade.strike_second_rem - trade.time_used) < 1){
                            trade.state = 'trading-on'; //TRADE START 
                        }
                    }else{
                        trade.close_price = data.close;
                        trade.end_data = data; //the last trading data that was received (it gets updated every tick from socket)

                        if(trade.time_used >= (trade.total_active_session_seconds)){
                            //Trading is done
                            data = {...trade.end_data};
                            trade.win = Trade.determineWinOrLose(data.close,trade);
                            trade.state = 'ended';
                            Trade.submitTrade(trade);
                        }
                        // else{
                        //     // STILL TRADING
                        //     if (trade.direction == 'up') {
                        //         if (data.close < trade.strike_price) {
                        //             trade.status = 'losing';
                        //         } else {
                        //             trade.status = 'winning';
                        //         }
                        //     } else if (trade.direction == 'down') {
                        //         if (data.close < trade.strike_price) {
                        //             trade.status = 'winning'; 
                        //         } else {
                        //             trade.status = 'losing';
                        //         }
                        //     }
                        // }
                    }
                }
                return trade;
            });

            if(trades[index].length > 0){
                Trade.notify(index,JSON.stringify({
                    action:'update_trades',trades:tr
                }));
            }
            
            all_trades = {...trades};
        }
    }
    static submitTrade(trade){
        var payload = {
            side: trade.direction == 'up' ? 1 : -1,
            account_type: trade.account_type,
            asset: trade.asset,
            open_time: Trade.convertToTime(trade.purchase_time_ms),
            close_time: Trade.convertToTime(trade.end_data.timestamp),
            open_price: trade.strike_price,
            close_price: trade.end_data.close,
            amount: trade.amount,
            profit: trade.profit,
            status: trade.win,
            percentage: trade.percentage_profit,
            seconds: trade.trade_time,
        };
        if (!trade.win) {
            payload.profit = -payload.amount;
        }
 
        // TRADE SUCCESS NOTIFICATION
        Trade.notify(trade.user_id,JSON.stringify({
            action:'trade_notification',
            type:'success',
            position:'top-left',
            message:trade.asset+' trade was completed successfully. Profit '+Trade.currencyProfitInUSD(payload.profit),
            trade:trade
        })); 

        // TRADE COMPLETED NOTIFICATION
        Trade.notify(trade.user_id,JSON.stringify({
            action:'trade_completed',
            trade:trade
        })); 

        var url = !trade.quick_demo ? '/update-trading-info': '/update-quick-trading-info';
        const options = {
            method:'POST',
            url: variables.API_ENDPOINT+url+"?token="+trade.user_token,
            form:payload,
            headers: {"Authorization" : "Bearer "+trade.user_token}
        };

        request(options,function (error, response, body){
            console.log(error);
            // console.log(body.substring(0,1000));
            
            if(!response) Trade.saveTradeError(trade,payload,error);

            if(response.statusCode == 200){
                var result = JSON.parse(body);
                Trade.removeCompletedTrade(trade);
                Trade.notify(trade.user_id,JSON.stringify({action:'update_user',data:result.data.user}));
                Trade.notify(trade.user_id,JSON.stringify({action:'update_trade_history',data:result.data.history}));
                
            }else{
                // UNABLE TO SAVE TRADE
                Trade.saveTradeError(trade,payload,error);
            }
        });
    }
    static saveTradeError(trade,payload,error){
        var error_list = fileservice.readOrResetJsonFile(variables.ERROR_FILE_PATH);
        error_list[trade.user_token] = error_list[trade.user_token] ? error_list[trade.user_token] : {}
        error_list[trade.user_token]['error'] = error;
        error_list[trade.user_token]['payload'] = payload;
        fileservice.writeFile(variables.ERROR_FILE_PATH, JSON.stringify(error_list));

        console.log('REMOVING TRADE DUE TO ERROR THAT OCCURED');
        Trade.removeCompletedTrade(trade);
    }
    static removeCompletedTrade(trade){
        var temp= {...all_trades};
        temp[trade.user_id] = all_trades[trade.user_id].filter((d)=> d.id != trade.id);
        all_trades = temp;
        console.log('Trade removed');
        fileservice.writeFile(variables.TRADES_FILE_PATH,JSON.stringify(all_trades));
    }
    static currencyProfitInUSD(amount){
        if(amount < 0){
            return '-$'+(amount  * -1);
        }
        return '+$'+amount;
    }
    static formatTime(min, seconds) {
        min = min < 10 ? '0' + min : min;
        seconds = seconds < 10 ? '0' + seconds : seconds;
        return min + ':' + seconds;
    }
    static convertToTime(time) {
        var date = new Date(time);
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }
    static determineWinOrLose(close, trade) {
        if ((trade.direction == 'up' && close > trade.strike_price) || (trade.direction == 'down' && close < trade.strike_price)) {
            return true;
        } else {
            return false;
        }
    }
    static timeDiferenceInSeconds(time1,time2){
        return (time1 - time2) / 1000;
    }
    static notify(id,data){
        if(users_connection_instances[id]){
            var instances = users_connection_instances[id];
            instances.forEach((instance)=>{
                instance.send(data);
            });
        }
    }
    static updateUserConnectionInstances(data){
        users_connection_instances = data;
    }
}     