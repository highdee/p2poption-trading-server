var path = require('path');

module.exports = {
    JSON_FILE_PATH : path.resolve('data/history.json'),
    TRADES_FILE_PATH : path.resolve('data/trades.json'),
    ERROR_FILE_PATH : path.resolve('data/errors.json'),
    ARRAY_SIZE_LIMIT : 500,
    API_ENDPOINT : 'http://localhost:8000/api/v1'
    // API_ENDPOINT : 'https://tradeadmin.optionp2p.io/api/v1'
}